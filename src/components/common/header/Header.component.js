import React from 'react';
import {Link, IndexLink} from 'react-router';
import './Header.scss';

const Header = () => {
    return (
        <header className="site-header">
            <nav className="site-header-nav navbar navbar-toggleable-md navbar-inverse  fixed-top">
                <div className="navbar-brand">
                    <IndexLink to="/" activeClassName="active">Home</IndexLink>
                </div>
                <div className="collapse navbar-collapse">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item"><Link to="/profile" activeClassName="active">Profile</Link></li>
                    </ul>

                </div>
            </nav>
        </header>
    );
}



export default Header;