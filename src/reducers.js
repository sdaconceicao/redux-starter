import {combineReducers} from 'redux';
import profile from './components/profile/Profile.reducers';

export const rootReducer = combineReducers({profile});
