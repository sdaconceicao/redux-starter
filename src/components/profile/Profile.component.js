import React, {PropTypes, Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import TextInput from './../common/textInput/TextInput.component';
import * as actions from './Profile.actions';
import Toast from 'grommet/components/Toast';
import Notification from 'grommet/components/Notification';

export class Profile extends Component {
    constructor(props, context) {
        super(props, context);
        this.save = this.save.bind(this);
        this.onChange = this.onChange.bind(this);
        this.state = {profile: Object.assign({}, props.profile), showToast: false};
    }

    componentWillReceiveProps(nextProps) {
        this.setState({profile: Object.assign({}, nextProps.profile)});
    }

    render() {
        const {profile, showToast} = this.state;

        let toastNode;
        if (showToast) {
            toastNode = (<Toast status='ok' onClose={() => this.setState({ showToast: false })}>Saved</Toast>);
        }
        return (
            <div>
                <h1>Profile</h1>

                <Notification state='Sample state'
                              message='Sample message'
                              size='medium'
                              status='warning' />
                { toastNode }
                { profile &&
                <form>
                    <TextInput
                        name="name"
                        label="Name"
                        value={profile.name}
                        onChange={this.onChange}/>

                    <TextInput
                        name="age"
                        label="Age"
                        value={profile.age}
                        onChange={this.onChange}/>
                   <input type="button"  onClick={this.save} value="Save"/>
                </form>
                }
            </div>
        );
    }

    onChange(event){
        const field = event.target.name;
        let profile = this.state.profile;
        profile[field] = event.target.value;
        return this.setState({profile});
    }
    save(){
        this.props.actions.updateProfile(this.state.profile).then(()=>{
            this.setState({showToast:true});
        });
    }
}

Profile.propTypes = {
    profile: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
    return {
        profile: state.profile
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(actions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);