import mock from './Profile.mock';

export default class ProfileApi{
    static getProfile(){
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(mock);
            }, 1000);
        });
    }

    static updateProfile(profile){
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(Object.assign({}, profile));
            }, 1000);
        });
    }
}