import React, { Component, PropTypes } from 'react';
import Grommet from 'grommet/components/App';
import Header from './components/common/header/Header.component';
import AccessSignIcon from 'grommet/components/icons/base/Clock';
import Pulse from 'grommet/components/icons/Pulse';
import './App.scss';

class App extends Component {
  render() {
    return (

        <Grommet>
            <Header>Heady</Header>
            <div className="container">
                <aside className="col-lg-2 col-md-2 col-sm-2">
                    Sidebar
                    <AccessSignIcon size="small" colorIndex="brand"/>
                    <Pulse />
                </aside>
                <div className="col-lg-10 col-md-10 col-sm-10">
                    <div> {this.props.children}</div>
                </div>

            </div>
        </Grommet>
    );
  }
}

App.propTypes = {
    children: PropTypes.object.isRequired
};


export default App;
