import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import Store from './store';
import Routes from './routes';
import {loadProfile} from './components/profile/Profile.actions';


const StoreInstance = Store();

StoreInstance.dispatch(loadProfile());
ReactDOM.render(
    <Provider store={StoreInstance}>
        {Routes}
    </Provider>,
    document.getElementById('root')
);
