import React from 'react';
import {IndexRoute} from 'react-router';
import Home from './Home.component';

export default (
    <IndexRoute component={Home} />
);