import profileApi from './Profile.api';

export const actions = {
    LOAD_PROFILE_SUCCESS: 'LOAD_PROFILE_SUCCESS',
    UPDATE_PROFILE_SUCCESS: 'UPDATE_PROFILE_SUCCESS'
};

export function loadProfileSuccess(profile) {
    return { type: actions.LOAD_PROFILE_SUCCESS, profile}
}

export function updateProfileSuccess(profile) {
    return { type: actions.UPDATE_PROFILE_SUCCESS, profile}
}

export function loadProfile(){
    return function(dispatch){
        return profileApi.getProfile().then(profile =>{
            dispatch(loadProfileSuccess(profile));
        }).catch(error=>{
            throw(error);
        })
    }
}

export function updateProfile(profile){
    return function(dispatch){
        return profileApi.updateProfile(profile).then(profile =>{
            dispatch(updateProfileSuccess(profile));
        }).catch(error=>{
            throw(error);
        })
    }
}
