import React from 'react';
import {Route, Router, browserHistory} from 'react-router';
import App from './App';
import homeRoutes from './components/home/Home.routes';
import profileRoutes from './components/profile/Profile.routes';

const Routes = (
    <Router history={browserHistory}>
        <Route path="/" component={App}>
            {homeRoutes}
            {profileRoutes}
        </Route>
    </Router>
);

export default Routes;