import React from 'react';
import * as profileActions from './Profile.actions';
import expect from 'expect';

describe('Profile actions', function(){
    it('creates am update profile action', ()=>{
        const profile = {name: 'john smith'};
        const expectedAction = {
            type: profileActions.actions.UPDATE_PROFILE_SUCCESS,
            profile
        };

       const action = profileActions.updateProfileSuccess(profile);
       expect(action).toEqual(expectedAction);
    });
});