import {actions} from './Profile.actions';

export default function profileReducers ( state= {}, action){
    switch (action.type){
        case actions.LOAD_PROFILE_SUCCESS:
            return action.profile;
        case actions.UPDATE_PROFILE_SUCCESS:
            return action.profile;
        default:
            return state;
    }
}
