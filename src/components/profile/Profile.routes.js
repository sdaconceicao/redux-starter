import React from 'react';
import {Route} from 'react-router';
import Profile from './Profile.component';

export default (
    <Route path="profile" component={Profile} />
);
